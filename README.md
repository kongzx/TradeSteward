# TradeSteward

#### 介绍
    贸易管理(ERP)是一款基于互联网应用模式并适用于商贸行业的经营管理软件，它集进货、销售、库存、账务和客户管理等为一体，帮助商贸行业客户处理日常的业务经营管理事项，有效辅助商贸企业解决业务流程各环节的执行和监控，并及时获取相关统计数据信息。同时，智慧商贸进销存为广大商户提供有针对性的行业资讯、渠道信息等服务，助力商户快速发展。
    目前专注云管货+云管账。主要模块有云管货、云管账、云管客、云分析、云营销、基础资料、系统管理等。支持预付款、收入支出、仓库调拨、组装拆卸、商品套餐等特色功能。拥有库存状况、出入库统计等报表。同时对角色和权限进行了细致全面控制，精确到每个按钮和菜单。

#### 项目展示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0610/214235_370377a2_2256339.png "微信图片_20200610213754.png")
![![输入图片说明](https://images.gitee.com/uploads/images/2020/0610/214300_2efd0a5b_2256339.png "微信图片_20200610213813.png")]
![输入图片说明](https://images.gitee.com/uploads/images/2020/0610/214440_5e450a0a_2256339.png "微信图片_20200610213819.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0610/214457_a5243aee_2256339.png "微信图片_20200610213845.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0610/214506_34791440_2256339.png "微信图片_20200610213851.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0610/214513_5c3d03ed_2256339.png "微信图片_20200610213904.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0610/214521_f2f34af2_2256339.png "微信图片_20200610213914.png")

#### 软件架构

    本项目的主要使用者为客户,即各行各业的老板，生意人，作为需求方。针对目前的自创业个体户当仓库较大，存储商品较多时，改善客户商品存储体验也属项目范畴。该项目通过优化页面，简化操作，来使用户采用尽可能少的操作，尽可能简单的流程完成业务，并在存储商品过程中实时查询。

    仓库管理人员 为了方便协调仓库的管理与商品的管理以及客户分析和各种报表，采用总部+多级网点的形式，通过不同的权限设定与功能模块让不同的网点连接起来形成固定的工作线，并且各个网点各司其职，形成良好的协同效果。针对管理人员的功能模块以简单，实用性为主要	目标，主要是为了让使用者更轻松，清晰地实现管理。


    1. 这里是列表文本货物管理，主要是商品的入库、出库历史以及库存盘点；
    2.账户管理，日常收支，客户应收账款，供应商应收账款，资金流水，账户转账；
    3.客户管理，客户等级价和供应商管理；
    4.分析统计，各种报表，如进货报表，销售报表，利润报表，业绩报表可以导出成Excel表格；
    5.营销方案，各种商品套餐和商品模板；
    6.基础资料，包含商品列表，商品分类，属性设置，单位设置，仓库管理；
    7.系统设置，业务设置，员工管理，角色管理；

### 技术选型

#### 后端技术:
技术 | 名称 | 官网
----|------|----
Spring Framework | 容器  | [http://projects.spring.io/spring-framework/](http://projects.spring.io/spring-framework/)
SpringMVC | MVC框架  | [http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#mvc](http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#mvc)
Apache Shiro | 安全框架  | [http://shiro.apache.org/](http://shiro.apache.org/)
MyBatis | ORM框架  | [http://www.mybatis.org/mybatis-3/zh/index.html](http://www.mybatis.org/mybatis-3/zh/index.html)
MyBatis Generator | 代码生成  | [http://www.mybatis.org/generator/index.html](http://www.mybatis.org/generator/index.html)
Druid | 数据库连接池  | [https://github.com/alibaba/druid](https://github.com/alibaba/druid)
Thymeleaf | 模板引擎  | [http://www.thymeleaf.org/](http://www.thymeleaf.org/)
Redis | 分布式缓存数据库  | [https://redis.io/](https://redis.io/)
Quartz | 作业调度框架  | [http://www.quartz-scheduler.org/](http://www.quartz-scheduler.org/)
Log4J | 日志组件  | [http://logging.apache.org/log4j/1.2/](http://logging.apache.org/log4j/1.2/)
Swagger2 | 接口测试框架  | [http://swagger.io/](http://swagger.io/)
Jenkins | 持续集成工具  | [https://jenkins.io/index.html](https://jenkins.io/index.html)
Maven | 项目构建管理  | [http://maven.apache.org/](http://maven.apache.org/)
